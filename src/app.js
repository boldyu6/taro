import Taro, { Component } from "@tarojs/taro";
import { Provider } from "@tarojs/redux";

import Index from "./pages/index";

import configStore from "./store";

import "./app.scss";

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

const store = configStore();

class App extends Component {
  config = {
    pages: [
      "pages/home/home",
      "pages/cart/cart",
      "pages/detail/detail",
      "pages/user/user",
      "pages/user/order/orderMsg",
      "pages/user/order/orderList",
      "pages/address/address",
      "pages/search/search",
      "pages/login/login",
      "pages/cate/cate",
      "pages/cate-sub/cate-sub",
      "pages/webview/webview",
    ],
    window: {
      backgroundTextStyle: "light",
      navigationBarBackgroundColor: "#fff",
      navigationBarTitleText: "呔熊",
      navigationBarTextStyle: "black"
    },
    tabBar: {
      color: "#666",
      selectedColor: "#b4282d",
      backgroundColor: "#fafafa",
      borderStyle: "black",
      list: [
        {
          pagePath: "pages/home/home",
          iconPath: "./assets/tab-bar/home.png",
          selectedIconPath: "./assets/tab-bar/home-active.png",
          text: "首页"
        },
        {
          pagePath: "pages/cate/cate",
          iconPath: "./assets/tab-bar/cate.png",
          selectedIconPath: "./assets/tab-bar/cate-active.png",
          text: "分类"
        },
        {
          pagePath: "pages/cart/cart",
          iconPath: "./assets/tab-bar/cart.png",
          selectedIconPath: "./assets/tab-bar/cart-active.png",
          text: "购物车"
        },
        {
          pagePath: "pages/user/user",
          iconPath: "./assets/tab-bar/user.png",
          selectedIconPath: "./assets/tab-bar/user-active.png",
          text: "我的"
        }
      ]
    }
  };

  componentDidMount() {
    wx.hideTabBar();

    const res = Taro.getSystemInfoSync();
    if (
      /iphone\sx/i.test(res.model) ||
      (/iphone/i.test(res.model) && /unknown/.test(res.model)) ||
      /iphone\s11/.test(res.model)
    ) {
      Taro.setStorageSync("isIphoneX", true);
    } else {
      Taro.setStorageSync("isIphoneX", false);
    }
  }

  componentDidShow() {}

  componentDidHide() {}

  componentCatchError() {}

  componentDidCatchError() {}

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render() {
    return (
      <Provider store={store}>
        <Index />
      </Provider>
    );
  }
}

Taro.render(<App />, document.getElementById("app"));
