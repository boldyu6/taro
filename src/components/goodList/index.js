import Taro, { Component } from '@tarojs/taro'
import { Button, Text, View, Image } from '@tarojs/components'
import './index.scss'
import '../../styles/flex.scss'

export default class goodList extends Component {
    state = {
        goodList: [{
            img: require('../../assets/demo1.png'),
            text: 'ami限量礼盒装全球限量 提亮保湿 q弹保湿 超补水',
            nowPrice: '￥150',
            oldPrice: '￥298'
        }, {
            img: require('../../assets/demo1.png'),
            text: 'ami限量礼盒装全球限量 提亮保湿 q弹保湿 超补水',
            nowPrice: '￥150',
            oldPrice: '￥298'
        }]
    }
    render() {
        return (
            <View className="flex-row">
                {goodList.map(items => {
                    return (
                        <View key={items} className="goods flex-col flex-y-center">
                            <View className="img-view flex-x-center flex-row">
                                <Image src={items.img} className="good-img" />
                            </View>
                            <View className="good-text">{items.text}</View>
                            <View className="flex-row flex-y-center  flex-x-start price-pre">
                                <View className="now-price">{items.nowPrice}</View>
                                <View className="old-price">{items.oldPrice}</View>
                            </View>
                        </View>
                    )
                })}

            </View>

        )
    }
}

