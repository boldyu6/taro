import Taro, { Component } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import "./index.scss";
import jump from "@utils/jump";

export default class TabBar extends Component {
  static defaultProps = {
    current: 1
  };

  state = {
    tabList: [
      {
        pagePath: "/pages/home/home",
        iconPath: "../../assets/tab-bar/home.png",
        selectedIconPath: "../../assets/tab-bar/home-active.png",
        text: "首页",
        key: 1
      },
      {
        pagePath: "/pages/cate/cate",
        iconPath: "../../assets/tab-bar/cate.png",
        selectedIconPath: "../../assets/tab-bar/cate-active.png",
        text: "分类",
        key: 2
      },
      {
        pagePath: "/pages/cart/cart",
        iconPath: "../../assets/tab-bar/cart.png",
        selectedIconPath: "../../assets/tab-bar/cart-active.png",
        text: "购物车",
        key: 3
      },
      {
        pagePath: "/pages/user/user",
        iconPath: "../../assets/tab-bar/user.png",
        selectedIconPath: "../../assets/tab-bar/user-active.png",
        text: "我的",
        key: 4
      }
    ]
  };

  changeTab = pagePath => {
    jump({ url: pagePath, method: "switchTab" });
  };

  render() {
    const { current } = this.props;
    const { tabList } = this.state;
    const isIphoneX = Taro.getStorageSync("isIphoneX");
    return (
      <View className={isIphoneX ? "tab flex-row isIphoneX" : "tab flex-row"}>
        {tabList.map(item => (
          <View
            key={item}
            className="tab_list flex-col flex-y-center"
            onClick={() => this.changeTab(item.pagePath)}
          >
            <Image
              className="tab_img"
              src={item.key == current ? item.selectedIconPath : item.iconPath}
            />
            <Text className="tab_text">{item.text}</Text>
          </View>
        ))}
      </View>
    );
  }
}
