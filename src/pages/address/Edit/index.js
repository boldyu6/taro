import Taro, { Component } from "@tarojs/taro";
import { View, Image } from "@tarojs/components";
import { AtFloatLayout, AtInput } from "taro-ui";

import "./index.scss";

import moreIcon from "@assets/more.png";

export default class Edit extends Component {
  static defaultProps = {
    visible: false
  };
  handleDefault = item => {};
  render() {
    const { visible } = this.props;
    return (
      <AtFloatLayout isOpened={visible}>
        <View className="add_modal">
          <View className="add_title">添加收货地址</View>
          <AtInput
            name="value1"
            title="收货人姓名"
            type="text"
            placeholder="请与身份证上姓名一致"
            placeholderStyle="font-size:14px"
            value={this.state.value1}
            onChange={(value, event) => this.handleChange(event)}
          />
          <AtInput
            name="value2"
            title="联系方式"
            type="phone"
            placeholder="请输入真实手机号码"
            value={this.state.value1}
            onChange={(value, event) => this.handleChange(event)}
          />
          <AtInput
            name="value2"
            title="省市"
            type="phone"
            placeholder="请选择省市区"
            value="666"
            editable={false}
            onClick={() => this.handleShowPicker()}
          >
            <Image src={moreIcon}></Image>
          </AtInput>
        </View>
      </AtFloatLayout>
    );
  }
}
