import Taro, { Component } from "@tarojs/taro";
import { View, Image, Text } from "@tarojs/components";
import "./index.scss";
import deleteIcon from "@assets/search_delete.png";
import gouIcon from "@assets/gou.png";

export default class meetPlace extends Component {
  static defaultProps = {
    list: []
  };
  handleDelete = item => {};
  handleDefault = item => {};
  render() {
    const { list } = this.props;
    return (
      <View className="flex-col">
        {list.map(item => (
          <View key={item} className="address_item">
            <View className="flex-x-between">
              <Text className="address_name">{item.address}</Text>
              <Text className="address_name">{item.mobile}</Text>
            </View>
            <View className="address_city flex-y-center">
              {item.current === 1 && <Text className="default">默认</Text>}
              <Text className="city">浙江省</Text>
              <Text className="city">杭州</Text>
              <Text className="city">西湖</Text>
            </View>
            <View className="flex-x-between">
              <View
                className="flex-y-center"
                onClick={() => this.props.onDefault(item)}
              >
                {item.current === 1 ? (
                  <Image className="gouIcon" src={gouIcon} />
                ) : (
                  <View className='noIcon'></View>
                )}
                设置默认
              </View>
              <Image
                className="deleteIcon"
                src={deleteIcon}
                onClick={() => this.props.onDelete(item)}
              />
            </View>
          </View>
        ))}
      </View>
    );
  }
}
