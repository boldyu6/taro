import Taro, { Component } from "@tarojs/taro";
import { View, Text, Image } from "@tarojs/components";
import "./address.scss";
import {  AtModal } from "taro-ui";

import List from "./List";
import Edit from "./Edit";

import fetch from "@utils/request";


class Home extends Component {
  config = {
    navigationBarTitleText: "我的"
  };

  state = {
    titleLike: "",
    isAdd: false,
    isDelete:false,
    formDate: {},
    list: [
      { address: "111", mobile: "123213124", current: 1 },
      { address: "222", mobile: "34243423" }
    ]
  };

  componentDidShow() {}

  getList = () => {
    const { titleLike } = this.state;
    fetch({
      url: "/buyer/item/list",
      method: "POST",
      payload: {
        titleLike: titleLike
      }
    }).then(res => {
      if (res) {
        this.setState({ loaded: true, login: true });
      } else {
        this.setState({ loaded: true, login: false });
      }
    });
  };

  handleDelete = (item) => {
    this.setState({
      isDelete:true,
      id:item.id
    })
  };
  handleDefault = item => {
    console.log(item);
  };
  handleChange = e => {
    const { formDate } = this.state;
    this.setState({
      ...formDate,
      [e.target.id]: e.target.value
    });
  };
  handleShowPicker = () => {
    console.log("显示picker");
  };

  render() {
    const { list, isAdd, isDelete } = this.state;
    return (
      <View className="address">
        <List
          list={list}
          onDelete={item => this.handleDelete(item)}
          onDefault={item => this.handleDefault(item)}
        />

        
        <View
          className="address_add"
          onClick={() => {
            this.setState({ isAdd: true });
          }}
        >
          新增地址
        </View>

        <Edit visible={isAdd}/>

        <AtModal
          isOpened={isDelete}
          cancelText="取消"
          confirmText="确定"
          onClose={this.handleClose}
          onCancel={this.handleCancel}
          onConfirm={this.handleConfirm}
          content="是否确认删除该地址？"
        />
      </View>
    );
  }
}

export default Home;
