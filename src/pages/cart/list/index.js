import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import { CheckboxItem, InputNumber } from '@components'

import './index.scss'
import "@styles/flex.scss"

import del_img from "@assets/search_delete.png"
import addIcon from "@assets/addIcon.png"
import subIcon from "@assets/subIcon.png"





export default class List extends Component {
  static defaultProps = {
    list: [],
    onUpdate: () => { },
    onUpdateCheck: () => { }
  }
  state = {
    shopList: [{
      shopName: '我家店铺',
      list: [{
        img: require('@assets/029.png'),
        text: '【限量版】乳霜爆款粉底液透亮肌乳霜爆款粉底液透亮…',
        type: '姨妈红 150ml',
        price: '￥150'
      }, {
        img: require('@assets/029.png'),
        text: '【限量版】乳霜爆款粉底液透亮肌乳霜爆款粉底液透亮…',
        type: '姨妈红 150ml',
        price: '￥150'
      }]
    }, {
      shopName: '我家店铺',
      list: [{
        img: require('@assets/029.png'),
        text: '【限量版】乳霜爆款粉底液透亮肌乳霜爆款粉底液透亮…',
        type: '姨妈红 150ml',
        price: '￥150'
      }]
    },
    {
      shopName: '我家店铺',
      list: [{
        img: require('@assets/029.png'),
        text: '【限量版】乳霜爆款粉底液透亮肌乳霜爆款粉底液透亮…',
        type: '姨妈红 150ml',
        price: '￥150'
      }, {
        img: require('@assets/029.png'),
        text: '【限量版】乳霜爆款粉底液透亮肌乳霜爆款粉底液透亮…',
        type: '姨妈红 150ml',
        price: '￥150'
      }, {
        img: require('@assets/029.png'),
        text: '【限量版】乳霜爆款粉底液透亮肌乳霜爆款粉底液透亮…',
        type: '姨妈红 150ml',
        price: '￥150'
      }, {
        img: require('@assets/029.png'),
        text: '【限量版】乳霜爆款粉底液透亮肌乳霜爆款粉底液透亮…',
        type: '姨妈红 150ml',
        price: '￥150'
      }]
    }]

  }

  getBaseItem = (item) => ({
    skuId: item.skuId,
    type: item.type,
    extId: item.extId,
    cnt: item.cnt,
    checked: item.checked,
    canCheck: true,
    promId: this.props.promId,
    promType: this.props.promType
  })

  handleUpdate = (item, cnt) => {
    const payload = {
      skuList: [{ ...this.getBaseItem(item), cnt }]
    }
    this.props.onUpdate(payload)
  }

  handleUpdateCheck = (item) => {
    const payload = {
      skuList: [{ ...this.getBaseItem(item), checked: !item.checked }]
    }
    this.props.onUpdateCheck(payload)
  }

  handleRemove = () => {
    // XXX 暂未实现左滑删除
  }

  checkSelect = (int,index) => {
    console.log(int,index)
  }

  deleOrder=(items)=>{
    console.log(items)
  }

  render() {
    const { list } = this.props
    const { shopList } = this.state
    return (
      <View className='cart-list'>


        {shopList.map((items, index) => {
          return (
            <View className="carts" key={index}>
              <View className="shop_name">{items.shopName}</View>
              {items.list.map((ite, inx) => {
                return (
                  <View className="flex-row flex-y-center carts-view" key={inx}>
                    <View className="carts-check" onClick={this.checkSelect.bind(this, inx, index)}></View>
                    <View className="carts-content flex-row">
                      <Image src={ite.img} className="good_img" />
                      <View className="flex-col carts-content-right">
                        <View className="flex-row flex-x-between">
                          <View className="carts-title">{ite.text}</View>
                          <Image className="delet-icon" src={del_img} onClick={this.deleOrder.bind(this, ite)} />
                        </View>
                        <Text className="carts-type">{ite.type}</Text>
                        <View className="flex-row flex-x-between carts-content-bot">
                          <Text className="carts-price">{ite.price}</Text>
                          <View className="flex-row flex-x-around flex-y-center">
                            <Image src={addIcon} className="symbol-icon" />
                            <View className="good-num">2</View>
                            <Image src={subIcon} className="symbol-icon" />
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                )
              })}
            </View>
          )

        })
        }




        {/* {list.map(item => (
          <View
            key={item.id}
            className='cart-list__item'
          >
            <CheckboxItem
              checked={item.checked}
              onClick={this.handleUpdateCheck.bind(this, item)}
            />
            <Image
              className='cart-list__item-img'
              src={item.pic}
            />
            <View className='cart-list__item-info'>
              <View className='cart-list__item-title'>
                {!!item.prefix &&
                  <Text className='cart-list__item-title-tag'>{item.prefix}</Text>
                }
                <Text className='cart-list__item-title-name' numberOfLines={1}>
                  {item.itemName}
                </Text>
              </View>

              <View className='cart-list__item-spec'>
                <Text className='cart-list__item-spec-txt'>
                  {item.specList.map(sepc => sepc.specValue).join(' ')}
                </Text>
              </View>

              <View className='cart-list__item-wrap'>
                <Text className='cart-list__item-price'>
                  ¥{item.actualPrice}
                </Text>
                <View className='cart-list__item-num'>
                  <InputNumber
                    num={item.cnt}
                    onChange={this.handleUpdate.bind(this, item)}
                  />
                </View>
              </View>
            </View>
          </View>
        ))} */}
      </View >
    )
  }
}
