import Taro, { Component } from '@tarojs/taro'
import { View, ScrollView } from '@tarojs/components'
import { Popup, Loading } from '@components'
import { connect } from '@tarojs/redux'
import * as actions from '@actions/item'
import { dispatchAdd } from '@actions/cart'
import { getWindowHeight } from '@utils/style'
import Gallery from './gallery'
import InfoBase from './info-base'
import InfoParam from './info-param'
import Detail from './detail'
import Footer from './footer'
import Spec from './spec'
import './detail.scss'

@connect(state => state.item, { ...actions, dispatchAdd })
class detail extends Component {
  config = {
    navigationBarTitleText: '商品详情'
  }

  constructor(props) {
    super(props)
    this.state = {
      selected: {},
      itemInfo : {
        "code": "200",
        "data": {
          "id": 1110003,
          "name": "全棉针织条纹四件套",
          "simpleDesc": "够柔够弹针织棉，亲肤可裸睡",
          "primaryPicUrl": "https://yanxuan-item.nosdn.127.net/9a33f08a3b0f5c06fdf4c586d51b2f7c.png",
          "listPicUrl": "https://yanxuan-item.nosdn.127.net/a380131018bacde5ce01162d0c5d230f.png",
          "primarySkuId": 1111023,
          "floorPrice": null,
          "ceilingPrice": 0,
          "status": 2,
          "soldOut": false,
          "underShelf": false,
          "itemDetail": {
            "detailHtml": "<p><img src=\"https://yanxuan-item.nosdn.127.net/9b3abb79d0ac62795edc719d241ecbba.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/9b3abb79d0ac62795edc719d241ecbba.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/b4647859b2814946e23ebffceeb2bdcf.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/b4647859b2814946e23ebffceeb2bdcf.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/20bf60d8ae56b130a4c16d23856657b4.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/20bf60d8ae56b130a4c16d23856657b4.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/ad581ea950639364c832dc6fbfc43bb6.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/ad581ea950639364c832dc6fbfc43bb6.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/ddeea306cf4932348afd31640f88d94c.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/ddeea306cf4932348afd31640f88d94c.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/b1ee2bd03219d7fa008e0f32ae57a439.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/b1ee2bd03219d7fa008e0f32ae57a439.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/b95ffbe6a3272e179282cd73bbbfca0c.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/b95ffbe6a3272e179282cd73bbbfca0c.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/e290c2c161d117973d58332b5f97429a.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/e290c2c161d117973d58332b5f97429a.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/f8d5656ce19399924fb10c522d3383dd.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/f8d5656ce19399924fb10c522d3383dd.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/911e2631c951697b19ce4e640b9fd5dd.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/911e2631c951697b19ce4e640b9fd5dd.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/68854579fc2f8a7767ac193aadf8bb18.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/68854579fc2f8a7767ac193aadf8bb18.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/ee35b4a823ac7e398f37908cf83f0087.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/ee35b4a823ac7e398f37908cf83f0087.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/7874facd012b95435f3f08712f7d95b1.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/7874facd012b95435f3f08712f7d95b1.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/9537d2b1c9ccceaa4240a87765de1962.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/9537d2b1c9ccceaa4240a87765de1962.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/450cc2897c6ccb191dbe6f87b7ec9fd1.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/450cc2897c6ccb191dbe6f87b7ec9fd1.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/80eacd58e28e25f91414c706d590cc1d.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/80eacd58e28e25f91414c706d590cc1d.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/1019f9ebf455de163d6811ba0442f58f.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/1019f9ebf455de163d6811ba0442f58f.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/7e78c39225cff1b4956e08a4d4d582f5.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/7e78c39225cff1b4956e08a4d4d582f5.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/67cd4704721fdeaae9630946bfaeb1e4.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/67cd4704721fdeaae9630946bfaeb1e4.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/73f6ef05b0b2030d5ffc35572afe5cde.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/73f6ef05b0b2030d5ffc35572afe5cde.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/022a89371b364c882f0e3995aa722807.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/022a89371b364c882f0e3995aa722807.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/2526b2be0763bcfb352e730280c96430.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/2526b2be0763bcfb352e730280c96430.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/e54d8f2066088ec60aeeb462b47baeba.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/e54d8f2066088ec60aeeb462b47baeba.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/eee9055c3a4f874013da78b7cdc57dcf.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/eee9055c3a4f874013da78b7cdc57dcf.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/509a6b357324ea7a1fe026e3fb746913.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/509a6b357324ea7a1fe026e3fb746913.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/550632cffd0c47709e8ef381d3e6540a.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/550632cffd0c47709e8ef381d3e6540a.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/b7f664ac45dd58afefc6b49779cbf0b6.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/b7f664ac45dd58afefc6b49779cbf0b6.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/e4c0b1217db906245e25cca6a3fae162.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/e4c0b1217db906245e25cca6a3fae162.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/b640c0c6b5b36e62d736348da51b9520.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/b640c0c6b5b36e62d736348da51b9520.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/816ad5e7cc3eb832777aa7799361b8f3.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/816ad5e7cc3eb832777aa7799361b8f3.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/b2a30dbba06215a78b54f311d46abdee.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/b2a30dbba06215a78b54f311d46abdee.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/adead0b38934935bb570df42204046a9.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/adead0b38934935bb570df42204046a9.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/06ea5905b5fec2fa30a6cf14a9957282.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/06ea5905b5fec2fa30a6cf14a9957282.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/1280f9483fff2a0b9d4a99cc77c847f5.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/1280f9483fff2a0b9d4a99cc77c847f5.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/5a8511b3fb1fbf90c49e72712c0f3980.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/5a8511b3fb1fbf90c49e72712c0f3980.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/0e82c4aaf4c6cd0a3aac20ce79e494ed.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/0e82c4aaf4c6cd0a3aac20ce79e494ed.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/42963c30c0d7029a4f455ce437bab762.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/42963c30c0d7029a4f455ce437bab762.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/81fb1dc2a79af65a6d328e4d8fc5d430.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/81fb1dc2a79af65a6d328e4d8fc5d430.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/b482f59efbbbd00670f63ea4caaa8761.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/b482f59efbbbd00670f63ea4caaa8761.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/6b584e50567b1f7b80dc0ed826500d03.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/6b584e50567b1f7b80dc0ed826500d03.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/0fa3ebb93101347046e2f53bedad83ef.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/0fa3ebb93101347046e2f53bedad83ef.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/26f38895caf7dadfa8c26a8eebae21c8.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/26f38895caf7dadfa8c26a8eebae21c8.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/bb364bc2a050efb3d7949fa4c98af069.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/bb364bc2a050efb3d7949fa4c98af069.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/cde005cc3570ec12ac83b2f6197745f8.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/cde005cc3570ec12ac83b2f6197745f8.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/b24cbb30bf2ec7885e06be373aea9b2d.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/b24cbb30bf2ec7885e06be373aea9b2d.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/2c66944edf06036cdabc6d634a3760f9.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/2c66944edf06036cdabc6d634a3760f9.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/90a573d960931983a3a22cf863507799.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/90a573d960931983a3a22cf863507799.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/e4b3e6e97f7d8a3372f01726c1ebb32b.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/e4b3e6e97f7d8a3372f01726c1ebb32b.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/abd45c338fa9423ecd95ca1746785141.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/abd45c338fa9423ecd95ca1746785141.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/07ba1e3480fa2cc8a6c8fc67562c32c6.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/07ba1e3480fa2cc8a6c8fc67562c32c6.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/3c6bc9405233508d9374c32c6bceb1f7.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/3c6bc9405233508d9374c32c6bceb1f7.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/7199dce9f53e2bf04eae08170edad4c0.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/7199dce9f53e2bf04eae08170edad4c0.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/8246066cd4d883d4b3b95600e27dbdd3.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/8246066cd4d883d4b3b95600e27dbdd3.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/e16e5d402931926388414ef8d765fc15.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/e16e5d402931926388414ef8d765fc15.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/b4173dfdae7a7c7072d7889c4f97e21f.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/b4173dfdae7a7c7072d7889c4f97e21f.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/7e9420d596bb01fa681f8f43f9c67500.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/7e9420d596bb01fa681f8f43f9c67500.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/4b07738886bdfb3ff329d6381333d8ab.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/4b07738886bdfb3ff329d6381333d8ab.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/5df11b623ccf8f5bafd378d58c81251e.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/5df11b623ccf8f5bafd378d58c81251e.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/29b2e51717dd019fa0aefeb89790fd7c.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/29b2e51717dd019fa0aefeb89790fd7c.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/5ca2a7e7a6c5a30236ec9a87221a3697.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/5ca2a7e7a6c5a30236ec9a87221a3697.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/c9d7facf4585f5db94c5025e40b6b6bc.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/c9d7facf4585f5db94c5025e40b6b6bc.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/469cba2298299becf40005d2be11d260.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/469cba2298299becf40005d2be11d260.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/3b904e06f90dae0a3d685e4ae7f66d52.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/3b904e06f90dae0a3d685e4ae7f66d52.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/e2d8ab4e10814e8b94500cd073ad9345.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/e2d8ab4e10814e8b94500cd073ad9345.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/a5890d71cc7b3404e06174e99d8bbacc.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/a5890d71cc7b3404e06174e99d8bbacc.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/3da57b097577909007e4d5cd32100959.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/3da57b097577909007e4d5cd32100959.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/15ea582c3f9fcd6cc59c6122d28d00ed.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/15ea582c3f9fcd6cc59c6122d28d00ed.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/30b4ab01b624b1c5ddeb20509c126984.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/30b4ab01b624b1c5ddeb20509c126984.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/781db2a447d69844c07aeb84eaa8ca0f.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/781db2a447d69844c07aeb84eaa8ca0f.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/842fe2f40ef3283d590c93cc6280a18f.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/842fe2f40ef3283d590c93cc6280a18f.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/d460df0829b03b90514efdc9e333af03.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/d460df0829b03b90514efdc9e333af03.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/c8f3e0423ed483941d23d26a156faf9b.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/c8f3e0423ed483941d23d26a156faf9b.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/960642ef18c48f7f01aef04e4db99a7e.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/960642ef18c48f7f01aef04e4db99a7e.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/67c5343ef8713625e851106a3ed2ce24.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/67c5343ef8713625e851106a3ed2ce24.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/3c77a86a0519819f44bfb79f657d016b.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/3c77a86a0519819f44bfb79f657d016b.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/c37813babf19b835131a19c5820bfb52.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/c37813babf19b835131a19c5820bfb52.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/b96048f5cf4807a98709dda195855432.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/b96048f5cf4807a98709dda195855432.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/9f830b0ffe5b8801e5c7a44723fdb2de.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/9f830b0ffe5b8801e5c7a44723fdb2de.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/f0452867640ae16337ef828a18756176.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/f0452867640ae16337ef828a18756176.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/7e5dadb8e2b8339d2592c0c36f6ff45d.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/7e5dadb8e2b8339d2592c0c36f6ff45d.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/7745e00d4324ee176ce824b4fb956fb6.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/7745e00d4324ee176ce824b4fb956fb6.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/a75ded175134a94a876eccd97386eed9.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/a75ded175134a94a876eccd97386eed9.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/708a87aec9d60c5b087b0554ed19383c.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/708a87aec9d60c5b087b0554ed19383c.jpg\" style=\"\"/></p><p><img src=\"https://yanxuan-item.nosdn.127.net/a2f358965fc93a1abf8e6952a08ad004.jpg\" _src=\"https://yanxuan-item.nosdn.127.net/a2f358965fc93a1abf8e6952a08ad004.jpg\" style=\"\"/></p><p><br/></p>",
            "picUrl1": "https://yanxuan-item.nosdn.127.net/74617a033ccc9930c803c342875d6cb0.jpg",
            "picUrl2": "https://yanxuan-item.nosdn.127.net/4cad263325c502daf3cac7d0d547f686.png",
            "picUrl3": "https://yanxuan-item.nosdn.127.net/e5acbec7fd80df14337e2d31acd8f6e1.png",
            "picUrl4": "https://yanxuan-item.nosdn.127.net/3fde637e01423fbb114fc9d31aca12a4.png"
          },
          "attrList": [{
            "attrName": "适用床尺寸",
            "attrValue": "1.5m、1.8m"
          }, {
            "attrName": "面料",
            "attrValue": "全棉"
          }, {
            "attrName": "风格",
            "attrValue": "日式"
          }, {
            "attrName": "工艺",
            "attrValue": "色织"
          }, {
            "attrName": "款式",
            "attrValue": "床单式、床笠式"
          }, {
            "attrName": "适用季节",
            "attrValue": "冬季"
          }, {
            "attrName": "图案",
            "attrValue": "几何"
          }, {
            "attrName": "尺寸",
            "attrValue": "1.5米床品（床笠款）： 被套 200*230cm/ 枕套：48*74cm*2/ 床笠：150*200*28cm\n1.8米床品（床笠款）：被套 220*240cm/ 枕套：48*74cm*2/ 床笠：180*200*28cm\n1.5米床品（床单款）： 被套 200*230cm/ 枕套：48*74cm*2/ 床单：245*250cm\n1.8米床品（床单款）：被套 220*240cm/ 枕套：48*74cm*2/ 床单：245*270cm\n三件套适用于（1.2-1.35M床）：被套 150*200cm/ 床单 200*230cm/ 枕套 48*74cm"
          }, {
            "attrName": "执行标准",
            "attrValue": "GB/T22844-2009"
          }, {
            "attrName": "产品重量",
            "attrValue": "【床笠款】1.5m床：约2.48kg\n【床笠款】1.8m床：约2.58kg\n【床单款】1.5m床：约2.53kg\n【床单款】1.8m床：约2.78kg"
          }],
          "skuMap": {
            "1101014;1101019": {
              "id": 1111023,
              "primarySku": true,
              "sellVolume": 79,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "299",
              "activityPrice": "269",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得26积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "299",
                "activityPrice": "269",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1138024;1101020": {
              "id": 1150026,
              "primarySku": false,
              "sellVolume": 0,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "329",
              "activityPrice": "296",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": "售罄",
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得29积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "329",
                "activityPrice": "296",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "3859039;1101020": {
              "id": 300032063,
              "primarySku": false,
              "sellVolume": 85,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "329",
              "activityPrice": "296",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得29积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "329",
                "activityPrice": "296",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1635003;1101019": {
              "id": 1599003,
              "primarySku": false,
              "sellVolume": 490,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": "预计11月20日开始发货",
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "299",
              "activityPrice": "269",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得26积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "299",
                "activityPrice": "269",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1636004;1101020": {
              "id": 1599011,
              "primarySku": false,
              "sellVolume": 379,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": "预计11月20日开始发货",
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "329",
              "activityPrice": "296",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得29积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "329",
                "activityPrice": "296",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "3859037;1101019": {
              "id": 300032056,
              "primarySku": false,
              "sellVolume": 238,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "299",
              "activityPrice": "269",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得26积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "299",
                "activityPrice": "269",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1138023;1101020": {
              "id": 1150024,
              "primarySku": false,
              "sellVolume": 17,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "329",
              "activityPrice": "296",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得29积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "329",
                "activityPrice": "296",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "3859036;1101019": {
              "id": 300032053,
              "primarySku": false,
              "sellVolume": 366,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "299",
              "activityPrice": "269",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得26积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "299",
                "activityPrice": "269",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "3859038;1101020": {
              "id": 300032060,
              "primarySku": false,
              "sellVolume": 215,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "329",
              "activityPrice": "296",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得29积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "329",
                "activityPrice": "296",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1636003;1101020": {
              "id": 1599005,
              "primarySku": false,
              "sellVolume": 335,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": "预计11月20日开始发货",
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "329",
              "activityPrice": "296",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得29积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "329",
                "activityPrice": "296",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1101018;1101019": {
              "id": 1111031,
              "primarySku": false,
              "sellVolume": 79,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "299",
              "activityPrice": "269",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得26积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "299",
                "activityPrice": "269",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1635004;1101019": {
              "id": 1599009,
              "primarySku": false,
              "sellVolume": 369,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": "预计11月20日开始发货",
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "299",
              "activityPrice": "269",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得26积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "299",
                "activityPrice": "269",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1101014;1101020": {
              "id": 1111024,
              "primarySku": false,
              "sellVolume": 0,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": "预计12月21日开始发货",
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "329",
              "activityPrice": "296",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": "售罄",
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得29积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "329",
                "activityPrice": "296",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1138024;1101019": {
              "id": 1150025,
              "primarySku": false,
              "sellVolume": 108,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "299",
              "activityPrice": "269",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得26积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "299",
                "activityPrice": "269",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "3859037;1101020": {
              "id": 300032057,
              "primarySku": false,
              "sellVolume": 191,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "329",
              "activityPrice": "296",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得29积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "329",
                "activityPrice": "296",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1636003;1101019": {
              "id": 1599006,
              "primarySku": false,
              "sellVolume": 730,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": "预计11月20日开始发货",
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "299",
              "activityPrice": "269",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得26积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "299",
                "activityPrice": "269",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1101018;1101020": {
              "id": 1111032,
              "primarySku": false,
              "sellVolume": 0,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": null,
              "retailPriceV2": "329",
              "activityPrice": null,
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": "售罄",
              "promotionTag": null,
              "promForbiddenDesc": "所选规格不参与降价活动",
              "tagList": null,
              "pointsTip": "购买最高得32积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": null,
              "promoTips": null,
              "couponLimit": null,
              "promotionInfo": {
                "promotionList": [{
                  "id": 0,
                  "tag": "回馈金卡",
                  "name": "24元免费送，无门槛使用",
                  "targetUrl": "/pages/webview/index?url=https%3A%2F%2Fyou.163.com%2Fitem%2Fdetail%3Fid%3D3988084",
                  "promLimitDesc": null
                }],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": ["新人15元直减"],
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [{
                  "id": 0,
                  "tag": "回馈金卡",
                  "name": "24元免费送，无门槛使用",
                  "targetUrl": "/pages/webview/index?url=https%3A%2F%2Fyou.163.com%2Fitem%2Fdetail%3Fid%3D3988084",
                  "promLimitDesc": null
                }],
                "couponSimpleList": ["新人15元直减"]
              },
              "newcomerGiftInfo": {
                "couponName": "新人15元直减券",
                "activationCode": "f0148c3d4f029d333b11f6cd84975af5"
              },
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "3859036;1101020": {
              "id": 300032054,
              "primarySku": false,
              "sellVolume": 295,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "329",
              "activityPrice": "296",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得29积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "329",
                "activityPrice": "296",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1635004;1101020": {
              "id": 1599008,
              "primarySku": false,
              "sellVolume": 232,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": "预计11月20日开始发货",
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "329",
              "activityPrice": "296",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得29积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "329",
                "activityPrice": "296",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "3859038;1101019": {
              "id": 300032059,
              "primarySku": false,
              "sellVolume": 323,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "299",
              "activityPrice": "269",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得26积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "299",
                "activityPrice": "269",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1138023;1101019": {
              "id": 1150023,
              "primarySku": false,
              "sellVolume": 166,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "299",
              "activityPrice": "269",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得26积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "299",
                "activityPrice": "269",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "3859039;1101019": {
              "id": 300032062,
              "primarySku": false,
              "sellVolume": 260,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": null,
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "299",
              "activityPrice": "269",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得26积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "299",
                "activityPrice": "269",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1635003;1101020": {
              "id": 1599002,
              "primarySku": false,
              "sellVolume": 533,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": "预计12月21日开始发货",
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "329",
              "activityPrice": "296",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得29积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "329",
                "activityPrice": "296",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            },
            "1636004;1101019": {
              "id": 1599012,
              "primarySku": false,
              "sellVolume": 657,
              "valid": true,
              "itemSkuSpecValueList": null,
              "presell": false,
              "presellDesc": "预计11月20日开始发货",
              "purchaseAttribute": 0,
              "timePurchaseSkuInfo": null,
              "limitPurchaseCount": 0,
              "exclusiveSkuInfo": null,
              "limitedTag": "年货特惠",
              "retailPriceV2": "299",
              "activityPrice": "269",
              "pricePreFix": "价格: ¥",
              "disableBuyDesc": null,
              "promotionTag": {
                "type": 2,
                "desc": "年货特惠",
                "schemeUrl": ""
              },
              "promForbiddenDesc": null,
              "tagList": null,
              "pointsTip": "购买最高得26积分",
              "spmcBanner": null,
              "itemRewardVO": null,
              "bigPromotion": {
                "bannerType": 2,
                "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
                "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
                "promoTitle": "年货特惠",
                "promoSubTitle": "",
                "content": null,
                "subContent": null,
                "status": 2,
                "sellVolumeDesc": null,
                "sellVolumePercent": 0,
                "startTime": null,
                "countdown": 28348612,
                "schemeUrl": "",
                "retailPrice": "299",
                "activityPrice": "269",
                "styleType": 1,
                "spmcPromInfo": null,
                "ingCountdown": 0,
                "activityPriceExt": null,
                "maxDisplayTime": 432000000,
                "ingBannerTitleUrl": null,
                "ingBannerContentUrl": null,
                "sellVolume": 0,
                "satisfy": true
              },
              "promoTips": null,
              "couponLimit": "特价商品不可与优惠券叠加使用",
              "promotionInfo": {
                "promotionList": [],
                "countDesc": "促销",
                "shareDesc": null
              },
              "skuTitle": "全棉针织条纹四件套",
              "shoppingReward": null,
              "couponSimpleList": null,
              "priceDesc": null,
              "depositDetail": null,
              "rewardTag": null,
              "rewardTagDesc": null,
              "spmcText": "",
              "itemDiscount": {
                "promotionList": [],
                "couponSimpleList": null
              },
              "newcomerGiftInfo": null,
              "operationAttribute": 0,
              "orderMemoStatus": 0
            }
          },
          "skuSpecList": [{
            "id": 1099003,
            "name": "颜色",
            "type": 1,
            "skuSpecValueList": [{
              "id": 1636004,
              "skuSpecId": 1099003,
              "picUrl": "https://yanxuan-item.nosdn.127.net/d75e85bdbdb862e68d93b7a593e48313.png",
              "value": "肉桂粉（床单款）new"
            }, {
              "id": 1635004,
              "skuSpecId": 1099003,
              "picUrl": "https://yanxuan-item.nosdn.127.net/6d347d24eb2ebfcd6a828ed79f122175.png",
              "value": "肉桂粉（床笠款）new"
            }, {
              "id": 1636003,
              "skuSpecId": 1099003,
              "picUrl": "https://yanxuan-item.nosdn.127.net/6152124bec80a579af819dcea94f562b.png",
              "value": "浅波蓝（床单款）new"
            }, {
              "id": 1635003,
              "skuSpecId": 1099003,
              "picUrl": "https://yanxuan-item.nosdn.127.net/11765c93c6534a819bfe04afa306e1fc.png",
              "value": "浅波蓝（床笠款）new"
            }, {
              "id": 1138023,
              "skuSpecId": 1099003,
              "picUrl": "https://yanxuan-item.nosdn.127.net/337f8211b7a260513db2889b63923eb9.png",
              "value": "烟草绿（床单款）"
            }, {
              "id": 1138024,
              "skuSpecId": 1099003,
              "picUrl": "https://yanxuan-item.nosdn.127.net/d5c9a7cc02dfd2e7b2fcab07d6917979.png",
              "value": "木兰黄（床单款）"
            }, {
              "id": 1101014,
              "skuSpecId": 1099003,
              "picUrl": "https://yanxuan-item.nosdn.127.net/8ba0131c778d08323211d91c1576a4d6.png",
              "value": "烟草绿（床笠款）"
            }, {
              "id": 1101018,
              "skuSpecId": 1099003,
              "picUrl": "https://yanxuan-item.nosdn.127.net/f7acf198824d4a5c28677e74e44d493a.png",
              "value": "茶香粉（床笠款）"
            }, {
              "id": 3859036,
              "skuSpecId": 1099003,
              "picUrl": "https://yanxuan-item.nosdn.127.net/32e22c59264101f7df3e4dd6227401fd.png",
              "value": "细条纹木兰黄（床单款）"
            }, {
              "id": 3859037,
              "skuSpecId": 1099003,
              "picUrl": "https://yanxuan-item.nosdn.127.net/6d64ae71ad18d98eeef25e8f775e8a82.png",
              "value": "细条纹木兰黄（床笠款）"
            }, {
              "id": 3859038,
              "skuSpecId": 1099003,
              "picUrl": "https://yanxuan-item.nosdn.127.net/835d55a63861a99ffced78067df9541c.png",
              "value": "细条纹丁子灰（床单款）"
            }, {
              "id": 3859039,
              "skuSpecId": 1099003,
              "picUrl": "https://yanxuan-item.nosdn.127.net/5a68702be15f8eff61a36b2a2f0208db.png",
              "value": "细条纹丁子灰（床笠款）"
            }]
          }, {
            "id": 1099004,
            "name": "尺寸",
            "type": 0,
            "skuSpecValueList": [{
              "id": 1101019,
              "skuSpecId": 1099004,
              "picUrl": null,
              "value": "1.5m床:适用2mx2.3m被芯"
            }, {
              "id": 1101020,
              "skuSpecId": 1099004,
              "picUrl": null,
              "value": "1.8m床:适用2.2mx2.4m被芯"
            }]
          }],
          "skuMaxCount": 99,
          "comments": [{
            "skuInfo": ["烟草绿（床笠款）", "1.8m床:适用2.2mx2.4m被芯"],
            "frontUserName": "1****7",
            "frontUserAvatar": "https://yanxuan.nosdn.127.net/38ca255c4d902b0ce8239b76cd6a357d.jpg",
            "content": "柔软舒服，简约百搭，床笠款的优势就是不怕床单褶皱，严选的床品真的性价比非常高！很完美的一次购物。",
            "createTime": 1573543633620,
            "picList": ["https://yanxuan.nosdn.127.net/cd6bb4b98cbd722a8f62bebf2a4366af.jpg", "https://yanxuan.nosdn.127.net/7031ddbc90ceb9894dd2000aa132d384.jpg", "https://yanxuan.nosdn.127.net/9b04e974e6f30c4c16c236cbcf16d5a5.jpg", "https://yanxuan.nosdn.127.net/34ee0848c657d911dcf9b5ef7802fe35.jpg", "https://yanxuan.nosdn.127.net/cc5fc0c0c4de0bd82f024c60cf704922.jpg"],
            "kfReplyTitle": null,
            "kfReplyContent": null,
            "memLevel": 3,
            "appendCommentVO": null,
            "star": 5
          }],
          "extraPrice": "",
          "tagList": [],
          "promotionInfo": {
            "promotionList": [],
            "countDesc": "促销",
            "shareDesc": null
          },
          "policyList": [{
            "title": "部分地区无法配送",
            "content": "省份：Slovenia、Ireland、France、Latvia、Luxembourg、Canada、United Kingdom、Singapore、Japan、Lithuania、Greece、Italy、Finland、Korea、Germany、United States、Thailand、Cyprus、Belgium、Portugal、Poland、Austria、Denmark、Netherlands、Malaysia、Croatia、Spain、Romania、Hungary、Sweden、Slovakia、Estonia、Philippines、Switzerland"
          }, {
            "title": "不支持返回馈金",
            "content": "该商品不返还回馈金"
          }],
          "gift": false,
          "newOnShelf": false,
          "remarkTitle": null,
          "remarkSchemeUrl": "",
          "timePurchaseDetail": null,
          "appExclusiveFlag": false,
          "suitList": [],
          "skuList": null,
          "promotionTag": {
            "type": 2,
            "desc": "年货特惠",
            "schemeUrl": ""
          },
          "commentCount": "142250",
          "featureList": [{
            "icon": "https://yanxuan-item.nosdn.127.net/4e810f9cbb5ce3a86d710d7859cb5127.jpg",
            "text1": "长绒棉朵",
            "text2": "纤维柔长"
          }, {
            "icon": "https://yanxuan-item.nosdn.127.net/ce0a4bad041979f542288d9ba355138c.jpg",
            "text1": "百隆纱线",
            "text2": "朦胧色彩"
          }, {
            "icon": "https://yanxuan-item.nosdn.127.net/4e41dd39d4a733bb700ffac69ea25624.jpg",
            "text1": "针织工艺",
            "text2": "透气微弹"
          }],
          "itemPromValid": true,
          "defaultSelectSkuId": 1111023,
          "retailPrice": "299",
          "activityPrice": "269",
          "itemStar": {
            "goodCmtRate": "99.5% 好评",
            "star": 5
          },
          "pointsTip": "购买最高得26积分",
          "itemSizeTableFlag": false,
          "scheduleDeliveryStatus": 0,
          "spmcBanner": null,
          "bonusBanner": null,
          "giftCardItemFlag": false,
          "itemRewardVO": null,
          "feedbackPkgFlag": false,
          "feedbackBonusCardFlag": false,
          "couponLimit": "特价商品不可与优惠券叠加使用",
          "picMode": 1,
          "depositDetail": null,
          "adBanners": [],
          "rewardTag": null,
          "rewardTagDesc": null,
          "spmcText": "",
          "itemDiscount": {
            "promotionList": [],
            "couponSimpleList": null
          },
          "newcomerGiftInfo": null,
          "couponSimpleList": [],
          "pinInfo": null,
          "bigPromotion": {
            "bannerType": 2,
            "bannerTitleUrl": "https://yanxuan.nosdn.127.net/83516d73ecd395749e0f4b59176b0b10.png",
            "bannerContentUrl": "https://yanxuan.nosdn.127.net/aee487bbec398d6c790d2e2a320da5e6.png",
            "promoTitle": "年货特惠",
            "promoSubTitle": "",
            "content": null,
            "subContent": null,
            "status": 2,
            "sellVolumeDesc": null,
            "sellVolumePercent": 0,
            "startTime": null,
            "countdown": 28348612,
            "schemeUrl": "",
            "retailPrice": "299",
            "activityPrice": "269",
            "styleType": 1,
            "spmcPromInfo": null,
            "ingCountdown": 0,
            "activityPriceExt": null,
            "maxDisplayTime": 432000000,
            "ingBannerTitleUrl": null,
            "ingBannerContentUrl": null,
            "sellVolume": 0,
            "satisfy": true
          },
          "promLogo": null,
          "promoTips": [],
          "salePoint": null,
          "recommendReasons": ["新疆长绒棉朵，天生柔软", "仅售1/2建议价，更高性价比", "针织面料，高档内衣般裸睡感"],
          "staffRecommendEntrance": {
            "version": "1578067200000",
            "schemeUrl": "/pages/webview/index?url=https%3A%2F%2Fm.you.163.com%2Ftopic%2Fv1%2Fpub%2FMZee3MWrbs.html",
            "title": "此商品入选了网易员工精选好物！",
            "moreText": "发现更多",
            "canShow": true
          },
          "shoppingReward": null,
          "shoppingRewardRule": null,
          "versionForbidDesc": null
        }
      }
    }
    this.itemId = parseInt(this.$router.params.itemId)
  }

  componentDidMount() {
 
  }

  handleSelect = (selected) => {
    this.setState({ selected })
  }

  handleAdd = () => {
    // 添加购物车是先从 skuSpecValueList 中选择规格，再去 skuMap 中找 skuId，多个规格时用 ; 组合
    const { itemInfo } = this.state
    const { skuSpecList = [] } = itemInfo.data
    const { visible, selected } = this.state
    const isSelected = visible && !!selected.id && itemInfo.data.skuMap[selected.id]
    const isSingleSpec = skuSpecList.every(spec => spec.skuSpecValueList.length === 1)

    if (isSelected || isSingleSpec) {
      const selectedItem = isSelected ? selected : {
        id: skuSpecList.map(spec => spec.skuSpecValueList[0].id).join(';'),
        cnt: 1
      }
      const skuItem = itemInfo.data.skuMap[selectedItem.id] || {}
      const payload = {
        skuId: skuItem.id,
        cnt: selectedItem.cnt
      }
      this.props.dispatchAdd(payload).then(() => {
        Taro.showToast({
          title: '加入购物车成功',
          icon: 'none'
        })
      })
      if (isSelected) {
        this.toggleVisible()
      }
      return
    }

    if (!visible) {
      this.setState({ visible: true })
    } else {
      // XXX 加购物车逻辑不一定准确
      Taro.showToast({
        title: '请选择规格（或换个商品测试）',
        icon: 'none'
      })
    }
  }

  toggleVisible = () => {
    this.setState({
      visible: !this.state.visible,
      selected: {}
    })
  }

  render () {
  
    const {itemInfo} =this.state;
    const { itemDetail = {} } = itemInfo.data;
    const gallery = [
      itemInfo.listPicUrl,
      itemDetail.picUrl1, itemDetail.picUrl2, itemDetail.picUrl3, itemDetail.picUrl4
    ].filter(i => i)
    const height = getWindowHeight(false)
    // XXX RN 的 transform 写法不同，这块可以统一放到 @utils/style 的 postcss() 中处理
    const popupStyle = process.env.TARO_ENV === 'rn' ?
      { transform: [{ translateY: Taro.pxTransform(-100) }] } :
      { transform: `translateY(${Taro.pxTransform(-100)})` }

    return (
      <View className='item'>
        <ScrollView
          scrollY
          className='item__wrap'
          style={{ height }}
        >
          <Gallery list={gallery} />
          <InfoBase data={itemInfo.data} />
          <InfoParam list={itemInfo.data.attrList} />
          <Detail html={itemDetail.detailHtml} />
        </ScrollView>

        {/* NOTE Popup 一般的实现是 fixed 定位，但 RN 不支持，只能用 absolute，要注意引入位置 */}
        <Popup
          visible={this.state.visible}
          onClose={this.toggleVisible}
          compStyle={popupStyle}
        >
          <Spec
            data={itemInfo.data}
            selected={this.state.selected}
            onSelect={this.handleSelect}
          />
        </Popup>

        <View className='item__footer'>
          <Footer onAdd={this.handleAdd} />
        </View>
      </View>
    )
  }
}

export default detail
