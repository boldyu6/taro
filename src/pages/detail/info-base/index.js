import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './index.scss'
import "@styles/flex.scss"

import shareIcon from "../assets/share.png"
import collIcon from "../assets/collect.png"
import moreIcon from "@assets/more.png"
export default class InfoBase extends Component {
  static defaultProps = {
    data: {}
  }
  state = {
    list: [{
      img: require("../assets/demo.png"),
      text: '全球直采'
    },
    {
      img: require("../assets/demo.png"),
      text: '全球直采'
    }, {
      img: require("../assets/demo.png"),
      text: '全球直采'
    }, {
      img: require("../assets/demo.png"),
      text: '全球直采'
    }]
  }

  render() {
    const { data } = this.props
    const { itemStar = {}, tagList = [] } = data
    const { list } = this.state
    return (
      <View>
        <View className="flex-col infoBase">
          <View className="flex-row flex-x-between infoBase-price">
            <View className="flex-row flex-y-center">
              <View className="bg_text">门店价</View>
              <View className="red_font">￥150</View>
              <View className="del_font">￥298</View>
            </View>
            <View className="flex-row">
              <View className="flex-col flex-y-center">
                <Image src={collIcon} className="infoBase-price-icon" />
                <Text className="infoBase-price-text">收藏</Text>
              </View>
              <View className="flex-col flex-y-center left-pad">
                <Image src={shareIcon} className="infoBase-price-icon" />
                <Text className="infoBase-price-text">分享</Text>
              </View>
            </View>
          </View>

          <View className="good-taxes">预计税费 ¥12.3</View>

          <View className="flex-col">
            <View className="good-title">Armani 阿玛尼 Lip Magnet16年秋冬新款小胖丁丝绒哑光唇釉</View>
            <View className="good-detil">阿玛尼小胖丁超显色，超轻薄，超持久，能够打造超时尚的致美持久哑光唇妆。涂抹后，水分的“渐进式挥发”令色泽提升，带给你意想不到的高色彩饱和度的视觉体验想不到的高色彩饱和度的视觉…</View>
          </View>
        </View>

        <View className="flex-col good-send">
          <View className="flex-row flex-x-between bor-bot">
            <View className="good-send-type">保税直发</View>
            <View className="good-send-address">本商品将由杭州保税仓发货</View>
          </View>
          <View className="flex-row flex-y-center flex-x-between">
            <View className="flex-row">
              {list.map((items, index) => {
                return (
                  <View className="flex-row flex-y-center good-send-list" key={index}>
                    <Image src={items.img} className="good-send-img" />
                    <Text className="good-send-text">{items.text}</Text>
                  </View>
                )
              })}
            </View>
            <Image src={moreIcon} className="more-img " />
          </View>
        </View>


        <View className="flex-col good-send">
          <View className="flex-row flex-x-between flex-y-center bor-bot">
            <View className="good-send-type">保税直发</View>
            <View className="flex-row flex-y-center">
              <Text className="good-send-right">请选规格</Text>
              <Image src={moreIcon} className="more-img " />
            </View>
          </View>
          <View className="flex-row flex-x-between flex-y-center">
            <View className="good-send-type">运费</View>
            <View className="flex-row flex-y-center">
              <Text className="good-send-right">6.00</Text>
              <Image src={moreIcon} className="more-img " />
            </View>
          </View>
        </View>


      </View>
      // <View className='item-info-base'>
      //   <View className='item-info-base__header'>

      //     <View className='item-info-base__header-wrap'>
      //       <Text className='item-info-base__header-name'>{data.name}</Text>
      //       <Text className='item-info-base__header-desc'>{data.simpleDesc}</Text>
      //     </View>
      //     <View className='item-info-base__header-star'>
      //       <Text className='item-info-base__header-star-txt'>
      //         {`${parseFloat(itemStar.goodCmtRate) || 0}%`}
      //       </Text>
      //       <Text className='item-info-base__header-star-link'>{'好评率>'}</Text>
      //     </View>
      //   </View>

      //   <View className='item-info-base__price'>
      //     <Text className='item-info-base__price-symbol'>¥</Text>
      //     <Text className='item-info-base__price-txt'>
      //       {data.activityPrice || data.retailPrice}
      //     </Text>
      //     {!!data.activityPrice &&
      //       <Text className='item-info-base__price-origin'>
      //         ¥{data.retailPrice}
      //       </Text>
      //     }
      //   </View>

      //   {!!tagList.length &&
      //     <View className='item-info-base__tag'>
      //       {tagList.map(item => (
      //         <View key={item.id} className='item-info-base__tag-item'>
      //           <Text className='item-info-base__tag-item-txt'>{item.tagName}</Text>
      //           <Image className='item-info-base__tag-item-img' src={rightArrow} />
      //         </View>
      //       ))}
      //     </View>
      //   }
      // </View>

    )
  }
}
