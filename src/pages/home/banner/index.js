import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image } from '@tarojs/components'
import './index.scss'
import jump from "@utils/jump";

export default class SwiperBanner extends Component {
  static defaultProps = {
    list: []
  }
  state = {
    swiperIndex: 0
  }
  swiperChange = (e) => {
    this.setState({
      swiperIndex: e.target.current
    })
  }

  render() {
    const { list } = this.props
    return (
      <View className='home-banner'>
        <Swiper
          className='home-banner__swiper'
          circular
          // autoplay
          indicatorDots
          indicatorActiveColor='rgb(178, 42, 49)'
          // TODO 目前 H5、RN 暂不支持 previousMargin、nextMargin
          previousMargin='30px'
          nextMargin='30px'
          onchange={(e) => this.swiperChange(e)}
        >
          {list.map((item, index) => (
            <SwiperItem
              key={item}
              className='home-banner__swiper-item'
            >
              <Image
              mode="aspectFill"
                className={swiperIndex === index?'home-banner__swiper-item-img active':'home-banner__swiper-item-img'}
                src={item}
                onClick={()=>jump({url:'/pages/detail/detail'})}
              />
            </SwiperItem>
          ))}
        </Swiper>
      </View>
    )
  }
}
