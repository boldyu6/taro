import Taro, { Component } from "@tarojs/taro";
import { View, Text, Image, ScrollView } from "@tarojs/components";
import { TabBar } from "@components";

import { connect } from "@tarojs/redux";
import * as actions from "@actions/home";
import { dispatchCartNum } from "@actions/cart";
import { getWindowHeight } from "@utils/style";
import "./home.scss";

import Banner from "./banner";
import MeetPlace from "./meetPlace"
import Pin from "./pin"
import Sell from "./sell"
import jump from '@utils/jump'

import arrowRight from "@assets/tab-bar/cart.png";
import sortIcon from "./assets/sort.png"
import searchIcon from "./assets/search.png";
import titlIcon1 from "./assets/top1.png"
import titlIcon2 from "./assets/top2.png"
import titlIcon3 from "./assets/top3.png"


@connect(state => state.home, { ...actions, dispatchCartNum })
class Home extends Component {
  config = {
    navigationBarTitleText: "呔熊",
    navigationStyle: "custom"
  };

  state = {
    loaded: false,
    loading: false,
    lastItemId: 0,
    hasMore: true,
    bannerList: [
      "http://qnoss3.lanlanlife.com/6bd51779727dcbf9fc7ca0d91fbc2299_456x750.jpg",
      "http://qnoss.lanlanlife.com/7a192763cb0d6c5b45e0da14eb8e90d1.jpg",
      "http://qnoss2.lanlanlife.com/17cf76ccaec65134bf900622b24d32b5_1080x1620.jpeg"
    ]
  };

  componentDidMount() {

  }


  render() {
    const { bannerList } = this.state;
    const isIphoneX = Taro.getStorageSync("isIphoneX");

    return (
      <View className="home">


        <View className="home_custom">
          <View className="home_bg"></View>
          <View className={isIphoneX?'isIphoneX home_title' :'home_title'}>呔熊</View>
          <View className="home_shop flex-x-start">
            <Image className="home_shopImg" src={arrowRight} />
            <Text className="home_shopName">米拉的小电脑</Text>
          </View>
          <View className="home__search flex-row flex-y-center">
            <View className="home__search-wrap flex-1" onClick={()=>jump({url:'/pages/search/search'})}>
              <Image className="home__search-img" src={searchIcon} />
              <Text className="home__search-txt">{`搜索您想要的商品`}</Text>
            </View>
            <View className="home__search-right flex-col flex-y-center">
              <Image className="home__search-right-img" src={sortIcon} />
              <Text className="home__search-right-text">分类</Text>
            </View>
          </View>

          <ScrollView
            scrollY
            className="home__wrap"
            onScrollToLower={this.loadRecommend}
          >
            {/* style={{ height: getWindowHeight() }} */}
            <Banner list={bannerList} />
          </ScrollView>



          <View className="flex-row flex-x-around home-meet">
            <View className="flex-row flex-x-center flex-y-center">
              <Image src={titlIcon1} className="meet-title-img" />
              <Text className="meet-title-text">全球直采</Text>
            </View>
            <View className="flex-row flex-x-center flex-y-center">
              <Image src={titlIcon2} className="meet-title-img" />
              <Text className="meet-title-text">极速发货</Text>
            </View>
            <View className="flex-row flex-x-center flex-y-center">
              <Image src={titlIcon3} className="meet-title-img" />
              <Text className="meet-title-text">假一赔十</Text>
            </View>
            <View className="flex-row flex-x-center flex-y-center">
              <Image src={titlIcon1} className="meet-title-img" />
              <Text className="meet-title-text">售后无忧</Text>
            </View>
          </View>

        </View>

        <MeetPlace></MeetPlace>

        <Pin></Pin>

        <Sell></Sell>

        <TabBar current={1}></TabBar>
      </View>
    );
  }
}

export default Home;
