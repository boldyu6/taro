import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import './index.scss'
import meetIcon1 from "../assets/meet1.png"


export default class meetPlace extends Component {
    static defaultProps = {
        list: []
    }

    render() {
        const { list } = this.props
        return (
            <View className='meet-content flex-col'>
                <View className="flex-row flex-x-around meet-row">
                    <View className="flex-col flex-x-center flex-y-center">
                        <Image src={meetIcon1} className="meet-title-img" />
                        <Text className="meet-title-text">食品营养</Text>
                    </View>
                    <View className="flex-col flex-x-center flex-y-center">
                        <Image src={meetIcon1} className="meet-title-img" />
                        <Text className="meet-title-text">极速发货</Text>
                    </View>
                    <View className="flex-col flex-x-center flex-y-center">
                        <Image src={meetIcon1} className="meet-title-img" />
                        <Text className="meet-title-text">假一赔十</Text>
                    </View>
                    <View className="flex-col flex-x-center flex-y-center">
                        <Image src={meetIcon1} className="meet-title-img" />
                        <Text className="meet-title-text">售后无忧</Text>
                    </View>
                </View>
                <View className="flex-row flex-x-around meet-row">
                    <View className="flex-col flex-x-center flex-y-center">
                        <Image src={meetIcon1} className="meet-title-img" />
                        <Text className="meet-title-text">食品营养</Text>
                    </View>
                    <View className="flex-col flex-x-center flex-y-center">
                        <Image src={meetIcon1} className="meet-title-img" />
                        <Text className="meet-title-text">极速发货</Text>
                    </View>
                    <View className="flex-col flex-x-center flex-y-center">
                        <Image src={meetIcon1} className="meet-title-img" />
                        <Text className="meet-title-text">假一赔十</Text>
                    </View>
                    <View className="flex-col flex-x-center flex-y-center">
                        <Image src={meetIcon1} className="meet-title-img" />
                        <Text className="meet-title-text">售后无忧</Text>
                    </View>
                </View>
                {/* {list.map(item => (
            <SwiperItem
              key={item}
              className='home-banner__swiper-item'
            >
              <Image
                className='home-banner__swiper-item-img'
                src={item}
              />
            </SwiperItem>
          ))} */}
            </View>
        )
    }
}
