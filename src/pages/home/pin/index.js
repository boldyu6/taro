import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import './index.scss'
import moreIcon from "../assets/more.png"
import demo1 from "../assets/029.png"


export default class meetPlace extends Component {
    static defaultProps = {
        list: []
    }

    render() {
        const { list } = this.props
        return (
            <View className='pin-content flex-col'>
                <View className="flex-row pin-title flex-x-between">
                    <View className="flex-row flex-y-center">
                        <View className="pin-max-text">今日拼团</View>
                        <View className="pin-min-text">|   拼着买更便宜</View>
                    </View>
                    <View className="flex-row flex-y-center">
                        <View className="pin-min-text">更多</View>
                        <Image src={moreIcon} className="pin-more" />
                    </View>
                </View>



                <View className="flex-col">
                    <View className="flex-row good">
                        <Image src={demo1} className="good-img" />
                        <View className="flex-col flex-x-between good-right">
                            <View className="good-title">阿玛尼阿玛尼快啊叫客户吃饱撑的B超的B超的还差不多河北产的长岛冰茶的不长不短</View>
                            <View className="flex-row flex-x-between">
                                <View className="flex-row flex-y-center">
                                    <View className="good-pin-title">拼团价</View>
                                    <View className="good-price">0元</View>
                                </View>
                                <View className="good-join">去参团</View>
                            </View>
                        </View>
                    </View>
                    <View className="flex-row good">
                        <Image src={demo1} className="good-img" />
                        <View className="flex-col flex-x-between good-right">
                            <View className="good-title">阿玛尼玛尼快啊叫客户吃饱撑的B超的B超的还差不多河北产的长岛冰茶的不长不短</View>
                            <View className="flex-row flex-x-between">
                                <View className="flex-row flex-y-center">
                                    <View className="good-pin-title">拼团价</View>
                                    <View className="good-price">0元</View>
                                </View>
                                <View className="good-join">去参团</View>
                            </View>
                        </View>
                    </View>
                    <View className="flex-row good">
                        <Image src={demo1} className="good-img" />
                        <View className="flex-col flex-x-between good-right">
                            <View className="good-title">阿玛尼阿玛尼快啊叫客户吃饱撑的B超的B超的还差不多河北产的长岛冰茶的不长不短</View>
                            <View className="flex-row flex-x-between">
                                <View className="flex-row flex-y-center">
                                    <View className="good-pin-title">拼团价</View>
                                    <View className="good-price">0元</View>
                                </View>
                                <View className="good-join">去参团</View>
                            </View>
                        </View>
                    </View>

                </View>
            </View>
        )
    }
}
