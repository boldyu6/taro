import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import './index.scss'
import moreIcon from "../assets/more.png"
import demo1 from "../assets/029.png"

export default class meetPlace extends Component {
    static defaultProps = {
        list: []
    }

    render() {
        const { list } = this.props
        return (
            <View className='sell-content flex-col'>
                <View className="flex-row sell-title flex-x-between">
                    <View className="flex-row flex-y-center">
                        <View className="sell-max-text">品牌特卖</View>
                        <View className="sell-min-text">|   海外大牌云集</View>
                    </View>
                </View>

                <View className="flex-col brand">
                    <View className="flex-row flex-x-between brand-title">
                        <View className="flex-row">
                            <Image src={demo1} className="brand-img" />
                            <Text className="sell-max-text">阿玛尼专场</Text>
                        </View>
                        <View className="flex-row flex-y-center">
                            <View className="sell-min-text">更多</View>
                            <Image src={moreIcon} className="sell-more" />
                        </View>
                    </View>

                    <View className="flex-row flex-x-around">

                        <View className="flex-col good">
                            <Image src={demo1} className="good-img" />
                            <View className="good-title">【限量版】爆款乳霜粉底透亮肌肤保湿精华</View>
                            <View className="flex-row flex-y-center">
                                <View className="new-price">￥169</View>
                                <View className="old-price">￥198</View>
                            </View>
                        </View>
                        <View className="flex-col good">
                            <Image src={demo1} className="good-img" />
                            <View className="good-title">【限量版】爆款乳霜粉底透亮肌肤保湿精华</View>
                            <View className="flex-row flex-y-center">
                                <View className="new-price">￥169</View>
                                <View className="old-price">￥198</View>
                            </View>
                        </View>
                        <View className="flex-col good">
                            <Image src={demo1} className="good-img" />
                            <View className="good-title">【限量版】爆款乳霜粉底透亮肌肤保湿精华</View>
                            <View className="flex-row flex-y-center">
                                <View className="new-price">￥169</View>
                                <View className="old-price">￥198</View>
                            </View>
                        </View>

                    </View>
                </View>

                <View className="flex-col brand">
                    <View className="flex-row flex-x-between brand-title">
                        <View className="flex-row">
                            <Image src={demo1} className="brand-img" />
                            <Text className="sell-max-text">阿玛尼专场</Text>
                        </View>
                        <View className="flex-row flex-y-center">
                            <View className="sell-min-text">更多</View>
                            <Image src={moreIcon} className="sell-more" />
                        </View>
                    </View>

                    <View className="flex-row flex-x-around">

                        <View className="flex-col good">
                            <Image src={demo1} className="good-img" />
                            <View className="good-title">【限量版】爆款乳霜粉底透亮肌肤保湿精华</View>
                            <View className="flex-row flex-y-center">
                                <View className="new-price">￥169</View>
                                <View className="old-price">￥198</View>
                            </View>
                        </View>
                        <View className="flex-col good">
                            <Image src={demo1} className="good-img" />
                            <View className="good-title">【限量版】爆款乳霜粉底透亮肌肤保湿精华</View>
                            <View className="flex-row flex-y-center">
                                <View className="new-price">￥169</View>
                                <View className="old-price">￥198</View>
                            </View>
                        </View>
                        <View className="flex-col good">
                            <Image src={demo1} className="good-img" />
                            <View className="good-title">【限量版】爆款乳霜粉底透亮肌肤保湿精华</View>
                            <View className="flex-row flex-y-center">
                                <View className="new-price">￥169</View>
                                <View className="old-price">￥198</View>
                            </View>
                        </View>

                    </View>
                </View>

            </View>
        )
    }
}
