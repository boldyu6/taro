import Taro, { Component } from "@tarojs/taro";
import { View, Text, Image } from "@tarojs/components";
import { connect } from "@tarojs/redux";
import * as actions from "@actions/user";
import { CDN } from "@constants/api";
import "./login.scss";

import logoImg from "@assets/logo.png";
import fetch from "@utils/request";
import jump from "@utils/jump";

@connect(state => state.user, actions)
class UserLogin extends Component {
  config = {
    navigationBarTitleText: "登录"
  };
  state = {
    is_check: false
  };

  checkFile = () => {
    this.setState({
      is_check: !this.state.is_check
    });
  };
  agreeAuth = () => {
    Taro.getUserInfo().then(res => {
      const { errMsg, userInfo } = res;
      if (errMsg === "getUserInfo:ok") {
        wx.getSetting({
          success(res) {
            if (res.authSetting["scope.userInfo"]) {
              // 已经授权，可以直接调用 getUserInfo 获取头像昵称

              wx.login({
                success(res) {
                  console.log(res);
                  fetch({
                    url: "/login/wechat_mini_app",
                    method: "POST",
                    autoLogin: false,
                    payload: {
                      code: res.code
                    }
                  }).then(res => {
                    if (res) {
                      Taro.setStorageSync('token', res)
                      jump({ url:'/pages/user/user', method: "switchTab" });

                    }
                  });
                }
              });
            }
          }
        });

        // Taro.showToast({
        //   title: `微信昵称: ${userInfo.nickName}`,
        //   icon: 'none'
        // })
      } else {
        Taro.showToast({
          title: "授权失败",
          icon: "none"
        });
      }
    });
  };

  render() {
    let { is_check } = this.state;

    return (
      <View className="user-login flex-row flex-y-center flex-x-center">
        <View className="flex-col login-content flex-y-center">
          <Image className="logo-img" src={logoImg} />

          <Text className="tip-text">
            您暂时未获取微信授权，将无法正常使用小程序功能，请先阅读且同意《呔熊商城用户协议》后在授权登录。
          </Text>

          <View className="flex-row agree-content flex-y-center">
            <View
              className={
                is_check === true ? "agree-check agree-checked" : "agree-check"
              }
              onClick={this.checkFile}
            ></View>
            <Text className="agree-text flex-1">《呔熊商城用户协议》</Text>
          </View>

          <View className="flex-row flex-x-around">
            <View className="login-but flex-x-center flex-y-center">拒绝</View>
            <Button
              className="login-but flex-x-center flex-y-center allow"
              openType="getUserInfo"
              onGetUserInfo={() => this.agreeAuth()}
            >
              授权登录
            </Button>
          </View>
        </View>
      </View>
    );
  }
}

export default UserLogin;
