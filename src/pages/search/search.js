import Taro, { Component } from "@tarojs/taro";
import { View, Text, Image, Input } from "@tarojs/components";
import "./search.scss";

import GoodList from "@components/goodList"
import searchIcon from "@assets/search_search.png";
import deleteIcon from "@assets/search_delete.png";
import fetch from "@utils/request";

class Home extends Component {
  config = {
    navigationBarTitleText: "搜索"
  };

  state = {
    historyList: Taro.getStorageSync("history") || [],
    titleLike: ""
  };

  componentDidShow() {
  }

  getList = () => {
    const { titleLike } = this.state;
    fetch({
      url: "/buyer/item/list",
      method: "POST",
      payload: {
        titleLike: titleLike
      }
    }).then(res => {
      if (res) {
        this.setState({ loaded: true, login: true });
      } else {
        this.setState({ loaded: true, login: false });
      }
    });
  };
  searchChange = e => {
    const { historyList } = this.state;
    let arr = [];
    arr = historyList;
    arr.push(e.target.value);
    this.setState(
      {
        historyList: arr,
        titleLike: e.target.value
      },
      () => {
        Taro.setStorageSync("history", arr);
        this.getList()
      }
    );
  };

  handleDelete = () => {
    this.setState(
      {
        historyList: []
      },
      () => {
        Taro.setStorageSync("history", []);
      }
    );
  };

  render() {
    const { historyList } = this.state;
    return (
      <View className="search">
        <View className="home__search flex-row flex-y-center">
          <View className="home__search-wrap">
            <Image className="home__search-img" src={searchIcon} />
            <Input
              style="width:100%"
              className="home__search-txt"
              placeholder="搜索您想要的商品"
              onConfirm={e => this.searchChange(e)}
            ></Input>
          </View>
        </View>

        <View className="search_title flex-x-between">
          <View>搜索历史</View>
          <Image src={deleteIcon} onClick={() => this.handleDelete()} />
        </View>

        <View className="history">
          {historyList.map(item => (
            <Text key={item} className="history_item">
              {item}
            </Text>
          ))}
        </View>

        <GoodList></GoodList>

      </View>
    );
  }
}

export default Home;
