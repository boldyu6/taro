import Taro, { Component } from "@tarojs/taro";
import { View, Text, Image } from "@tarojs/components";
import jump from "@utils/jump";
import "./index.scss";

export default class Menu extends Component {
  state = {
    list1: [
      {
        title: "我的地址",
        img: require("../assets/menu1.png"),
        path: "/pages/address/address"
      },
      {
        title: "联系客服",
        img: require("../assets/menu2.png")
      },
      {
        title: "实名认证",
        img: require("../assets/menu3.png")
      },
      {
        title: "我的收藏",
        img: require("../assets/menu4.png")
      }
    ],
    list2: [
      {
        title: "我要开店",
        img: require("../assets/menu5.png")
      },
      {
        title: "设置",
        img: require("../assets/menu6.png")
      }
    ]
  };
  jumpTo = path => {
    path && jump({ url: path });
  };

  render() {
    const { list1, list2 } = this.state;
    return (
      <View className="user-menu flex-col">
        <View className="user-max-text">我的服务</View>
        <View className="flex-row  menu-list">
          {list1.map(items => {
            return (
              <View
                key={items}
                className="flex-col flex-y-center menu"
                onClick={() => this.jumpTo(items.path)}
              >
                <Image src={items.img} className="menu-img" />
                <Text className="menu-text">{items.title}</Text>
              </View>
            );
          })}
        </View>

        <View className="flex-row  menu-list">
          {list2.map(items => {
            return (
              <View
                key={items.title}
                className="flex-col flex-y-center menu"
                onClick={() => this.jumpTo(items.path)}
              >
                <Image src={items.img} className="menu-img" />
                <Text className="menu-text">{items.title}</Text>
              </View>
            );
          })}
        </View>
      </View>
    );
  }
}
