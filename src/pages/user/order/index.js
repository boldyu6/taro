import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'
import './index.scss'
import jump from '@utils/jump'
import moreIcon from "@assets/more.png"
import orderIcon1 from "../assets/orderIcon1.png"
import orderIcon2 from "../assets/orderIcon2.png"
import orderIcon3 from "../assets/orderIcon3.png"
import orderIcon4 from "../assets/orderIcon4.png"
import orderIcon5 from "../assets/orderIcon5.png"

export default class order extends Component {
    static defaultProps = {
        list: []
    }

    state = {
        orderTitle: [{
            title: '待付款',
            img: orderIcon1
        }, {
            title: '待分享',
            img: orderIcon2
        }, {
            title: '待收货',
            img: orderIcon3
        }, {
            title: '待发货',
            img: orderIcon4
        }, {
            title: '售后退款',
            img: orderIcon5
        }]
    }

    render() {
        const { list } = this.props
        return (
            <View className='order-content flex-col'>
                <View className="flex-row flex-x-between order-title">
                    <View className="flex-row">
                        <Text className="order-max-text">我的订单</Text>
                    </View>
                    <View className="flex-row flex-y-center" onClick={()=>jump({url:'/pages/user/order/orderList'})}>
                        <View className="order-min-text">查看全部</View>
                        <Image src={moreIcon} className="order-more" />
                    </View>
                </View>

                <View className="flex-row flex-x-around">
                    {orderTitle.map((item, index) => {
                        return (
                            <View key={index} className="flex-col flex-y-center">
                                <Image src={item.img} className="order-img" />
                                <Text className="order-title">{item.title}</Text>
                            </View>
                        )
                    })}
                </View>

            </View>
        )
    }
}
