import Taro, { Component } from "@tarojs/taro";
import { View, Image, Text } from "@tarojs/components";
import "./index.scss";

import searchIcon from "@assets/search_search.png";

import { AtTabs, AtTabsPane } from "taro-ui";

import jump from "@utils/jump";

export default class order extends Component {
    static defaultProps = {
        list: []
    };
    state = {
        current: 0,
        tabList: [{ title: '全部' }, { title: '待发货' }, { title: '待分享' }, { title: '待发货' }, { title: '待收货' }, { title: '售后退款' }],
        orderList: [{
            ordernum: '1234567890',
            statue: 0,
            list: [{
                text: 'ARMANI当红时刻限量礼盒 臻选粉底面部专用 150ml 3盒套装',
                img: require("@assets/gou.png"),
                type: '姨妈红 200ml',
                price: '￥150',
                num: '2',
            }, {
                text: 'ARMANI当红时刻限量礼盒 臻选粉底面部专用 150ml 3盒套装',
                img: require("@assets/gou.png"),
                type: '姨妈红 200ml',
                price: '￥150',
                num: '2',
            }],
            total: 2,
            mountPrice: '￥300.00',
        }, {
            ordernum: '1284567890',
            statue: 0,
            list: [{
                text: 'ARMANI当红时刻限量礼盒 臻选粉底面部专用 150ml 3盒套装',
                img: require("@assets/gou.png"),
                type: '姨妈红 200ml',
                price: '￥150',
                num: '2',
            }],
            total: 1,
            mountPrice: '￥150.00',
        }, {
            ordernum: '1234567890',
            statue: 0,
            list: [{
                text: 'ARMANI当红时刻限量礼盒 臻选粉底面部专用 150ml 3盒套装',
                img: require("@assets/gou.png"),
                type: '姨妈红 200ml',
                price: '￥150',
                num: '2',
            }, {
                text: 'ARMANI当红时刻限量礼盒 臻选粉底面部专用 150ml 3盒套装',
                img: require("@assets/gou.png"),
                type: '姨妈红 200ml',
                price: '￥150',
                num: '2',
            }],
            total: 2,
            mountPrice: '￥300.00',
        },]


    };

    handleClick = e => {
        this.setState({
            current: e
        });
    };

    searchOrder = e => {

    }

    toOrderMsg = e => {
        jump({ url: "/pages/user/order/orderMsg" })
    }

    render() {
        const { current, tabList, orderList } = this.state;
        return (
            <View className="flex-col">
                <View className="orderList__searchPre">
                    <View className="orderList__search flex-row flex-y-center">
                        <View className="orderList__search-wrap">
                            <Image className="orderList__search-img" src={searchIcon} />
                            <Input
                                style="width:100%"
                                className="orderList__search-txt"
                                placeholder="搜索商品名称、订单编号"
                                onConfirm={e => this.searchOrder(e)}
                            ></Input>
                        </View>
                    </View>
                </View>


                <AtTabs current={current} tabList={tabList} onClick={this.handleClick.bind(this)}>
                    {tabList.map((items, index) => {
                        return (
                            <AtTabsPane current={this.state.current} index={index} className="atTabs-pan"  >
                                {orderList.map((ite, inx) => {
                                    return (
                                        <View className="flex-col orderList" key={index + inx} onClick={() => this.toOrderMsg()}>
                                            <View className="flex-row flex-x-between orderList-top">
                                                <Text>订单号:{ite.ordernum}</Text>
                                                <Text>代付款</Text>
                                            </View>
                                            <View className="flex-col orderList-content">
                                                {ite.list.map((itms, ind) => {
                                                    return (
                                                        <View className="flex-row orderList-content-view" key={index + inx + ind}>
                                                            <Image src={itms.img} className="orderList-content-img" />
                                                            <View className="flex-col">
                                                                <View className="flex-row">
                                                                    <View className="orderList-content-title">{itms.text}</View>
                                                                    <View className="flex-col orderList-content-right flex-y-end">
                                                                        <View className="orderList-content-price">{itms.price}</View>
                                                                        <View className="orderList-content-num">x{itms.num}</View>
                                                                    </View>
                                                                </View>
                                                                <View className="orderList-content-speces">{itms.type}</View>
                                                            </View>
                                                        </View>
                                                    )
                                                })}
                                            </View>
                                            <View className="flex-row flex-x-end orderList-bottom">
                                                <Text>共{ite.total}件</Text>
                                                <Text>订单金额:{ite.mountPrice}</Text>
                                            </View>
                                            <View className="flex-row flex-x-end orderList-but">
                                                <View className="cancel-but flex-x-center">取消订单</View>
                                                <View className="pay-but flex-x-center">去支付</View>
                                            </View>
                                        </View>
                                    )
                                })}
                            </AtTabsPane>
                        )
                    })}
                </AtTabs>
            </View>

        );
    }
}
