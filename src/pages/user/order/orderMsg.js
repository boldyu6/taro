import Taro, { Component } from "@tarojs/taro";
import { View, Image, Text } from "@tarojs/components";
import "./index.scss";


import { AtTabs, AtTabsPane } from "taro-ui";

import jump from "@utils/jump";

import closeImg from "../assets/orderMsg-close.png";
import orderIcon from "../assets/order-icon.png";
import shopIcon from "../assets/orderMsg-shop.png";
import serviceIcon from "../assets/orderMsg-service.png";
import demoimg from "@assets/gou.png";








export default class order extends Component {
    static defaultProps = {
        list: []
    };
    state = {
    };

    // handleClick = e => {
    //     this.setState({
    //         current: e
    //     });
    // };

    // searchOrder = e => {

    // }

    // toOrderMsg = e => {
    //     jump({url: "pages/user/order/orderMsg"})
    // }

    render() {
        return (
            <View className="flex-col orderMsg">

                <View className="flex-row orderMsg-status flex-x-between">
                    <View className="flex-col">
                        <View className="flex-row flex-y-center">
                            <Image src={orderIcon} className="order-icon" />
                            <Text className="orderMsg-status-text">关闭交易</Text>
                        </View>
                        <View className="orderMsg-status-time">2018-05-20  19:21:26</View>
                    </View>
                    <Image src={closeImg} className="order-img" />
                </View>


                <View className="flex-col orderMsg-content">
                    <View className="flex-row orderMsg-content-top flex-x-between">
                        <View className="flex-row ">
                            <Image src={shopIcon} className="orderMsg-content-icon" />
                            <Text className="shopText">保税仓直发</Text>
                        </View>
                        <View className="flex-row">
                            <Image src={serviceIcon} className="orderMsg-content-icon" />
                            <Text className="serviceText">联系客服</Text>
                        </View>
                    </View>
                    <View className="flex-row orderList-content-view orderMsg-content-view">
                        <Image src={demoimg} className="orderList-content-img" />
                        <View className="flex-col">
                            <View className="flex-row">
                                <View className="orderList-content-title">ARMANI当红时刻限量礼盒 臻选粉底面部专用 150ml 3套装</View>
                                <View className="flex-col orderList-content-right flex-y-end">
                                    <View className="orderList-content-price">$150</View>
                                    <View className="orderList-content-num">x1</View>
                                </View>
                            </View>
                            <View className="orderList-content-speces">姨妈红 150ml</View>
                        </View>
                    </View>
                </View>



                <View className="flex-col orderMsg-price">
                    <View className="flex-row flex-x-between orderMsg-price-view">
                        <View className="block-text">订单金额</View>
                        <View className="red-text">¥150.00</View>
                    </View>
                    <View className="flex-row flex-x-between orderMsg-price-view">
                        <View>商品总价</View>
                        <View className="block-text">¥150.00</View>
                    </View>
                    <View className="flex-row flex-x-between orderMsg-price-view">
                        <View>运费</View>
                        <View className="block-text">¥150.00</View>
                    </View>
                    <View className="flex-row flex-x-between orderMsg-price-view">
                        <View>税费</View>
                        <View className="block-text">¥150.00</View>
                    </View>
                    <View className="flex-row flex-x-between orderMsg-price-view">
                        <View>备注</View>
                        <View >尽快发货</View>
                    </View>
                </View>


                <View className="flex-col orderMsg-other">
                    <View className="flex-row flex-x-between">
                        <Text className="orderMsg-other-text">订单编号:2019021222073345050367890</Text>
                        <View className="orderMsg-other-but flex-x-center flex-y-center">复制</View>
                    </View>
                    <View className="flex-row flex-x-start">
                        <Text className="orderMsg-other-text">支付方式:微信支付</Text>
                    </View>
                    <View className="flex-row flex-x-start">
                        <Text className="orderMsg-other-text">下单时间:2019-08-19 22:35:09</Text>
                    </View>
                </View>
            </View>

        );
    }
}
