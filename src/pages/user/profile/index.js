import Taro, { Component } from "@tarojs/taro";
import { View, Text, Image } from "@tarojs/components";
import defaultAvatar from "@assets/default-avatar.png";
import "./index.scss";

export default class Profile extends Component {
  static defaultProps = {
    userInfo: {}
  };

  handleLogin = () => {
    if (!this.props.userInfo.login) {
      Taro.navigateTo({
        url: "/pages/login/login"
      });
    }
  };



  render() {
    const { userInfo } = this.props;

    return (
      <View className="user-profile">

        <View className="user_bg flex-col flex-y-center">
          <View className="user_title">我的</View>
          <View className="user_avatar">
            <Image
              className="user-profile__avatar-img"
              src={userInfo.avatar || defaultAvatar}
              onClick={this.handleLogin}
            />
          </View>

          <Text className="user_name">name</Text>

        </View>

      </View>
    );
  }
}
