import Taro, { Component } from "@tarojs/taro";
import { View, Text, ScrollView } from "@tarojs/components";
import { connect } from "@tarojs/redux";
import { TabBar } from "@components";

import * as actions from "@actions/user";
import { dispatchCartNum } from "@actions/cart";
import Profile from "./profile";
import Menu from "./menu";
import Order from "./order";

import "./user.scss";

@connect(state => state.user, { ...actions, dispatchCartNum })
class User extends Component {
  config = {
    navigationBarTitleText: "我的",
    navigationStyle: "custom"
  };

  componentDidShow() {}

  handleLogin = () => {};

  render() {
    const { userInfo } = this.props;

    return (
      <View className="user">
        {/* <ScrollView
          scrollY
          className='user__wrap'
          style={{ height: getWindowHeight() }}
        >
          <Profile userInfo={userInfo} />
          {userInfo.login &&
            <View className='user__logout' onClick={this.handleLogin}>
              <Text className='user__logout-txt'>切换账号</Text>
            </View>
          }
          <View className='user__empty' />
        </ScrollView> */}

        <Profile userInfo={userInfo} />
        <View className="flex-col my-content">
          <Order></Order>

          <Menu></Menu>
        </View>

        <TabBar current={4} />
      </View>
    );
  }
}

export default User;
