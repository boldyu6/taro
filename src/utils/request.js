import Taro from "@tarojs/taro";

const CODE_SUCCESS = 200;
const CODE_AUTH_EXPIRED = "600";
const host = HOST;

/**
 * 简易封装网络请求
 * @param {*} options
 */
export default function fetch(options) {
  const {
    url,
    payload,
    method = "GET",
    showToast = true,
    autoLogin = true
  } = options;
  const token = Taro.getStorageSync("token");
  const header = token ? { cookie: "SESSION=" + token } : {};

  if (url === "/login/wechat_mini_app") {
    header["content-type"] = "application/x-www-form-urlencoded";
  }

  return Taro.request({
    url: host + url,
    method,
    data: payload,
    header
  })
    .then(res => {
      const { code, data } = res.data;
      if (code !== CODE_SUCCESS) {
        return Promise.reject(res.data);
      }

      return data;
    })
    .catch(err => {
      const defaultMsg =
        err.code === CODE_AUTH_EXPIRED ? "登录失效" : "请求异常";
      if (showToast) {
        Taro.showToast({
          title: (err && err.errorMsg) || defaultMsg,
          icon: "none"
        });
      }

      if (err.code === CODE_AUTH_EXPIRED && autoLogin) {
        Taro.navigateTo({
          url: "/pages/login/login"
        });
      }

      return Promise.reject({ message: defaultMsg, ...err });
    });
}
